package ru.t1.vlvov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.command.AbstractCommand;
import ru.t1.vlvov.tm.dto.Domain;
import ru.t1.vlvov.tm.enumerated.Role;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public final static String FILE_TEXT = "data.base64";

    @NotNull
    public final static String FILE_BINARY = "data.bin";

    @NotNull
    public final static String FILE_XML = "data.xml";

    @NotNull
    public final static String FILE_JSON = "data.json";

    @NotNull
    public final static String FILE_YAML = "data.yaml";

    @NotNull
    public final static String CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    public final static String CONTEXT_FACTORY_JAXB = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    public final static String MEDIA_TYPE = "eclipselink.media-type";

    @NotNull
    public final static String APPLICATION_TYPE_JSON = "application/json";

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    protected Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        domain.setUsers(serviceLocator.getUserService().findAll());
        return domain;
    }

    protected void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        serviceLocator.getProjectService().set(domain.getProjects());
        serviceLocator.getTaskService().set(domain.getTasks());
        serviceLocator.getUserService().set(domain.getUsers());
        serviceLocator.getAuthService().logout();
    }

}
