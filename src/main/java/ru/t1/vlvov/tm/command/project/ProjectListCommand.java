package ru.t1.vlvov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.enumerated.Sort;
import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    private final String DESCRIPTION = "Display all projects.";

    @NotNull
    private final String NAME = "project-list";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("SELECT SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sortName = TerminalUtil.nextLine();
        @Nullable final Sort sort = Sort.toSort(sortName);
        @Nullable final String userId = getAuthService().getUserId();
        @NotNull final List<Project> projects = getProjectService().findAll(userId, sort);
        for (@Nullable final Project project : projects) {
            showProject(project);
        }
    }

}