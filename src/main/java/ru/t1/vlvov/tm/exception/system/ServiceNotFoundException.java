package ru.t1.vlvov.tm.exception.system;

import org.jetbrains.annotations.Nullable;

public class ServiceNotFoundException extends AbstractSystemException {

    public ServiceNotFoundException() {
        super("Error! Service not defined...");
    }

}