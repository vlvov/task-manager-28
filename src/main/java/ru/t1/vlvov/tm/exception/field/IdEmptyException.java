package ru.t1.vlvov.tm.exception.field;

public final class IdEmptyException extends AbstractFieldNotFoundException {

    public IdEmptyException() {
        super("Error! Id is empty...");
    }

}